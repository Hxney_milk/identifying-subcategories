WITH SubcategorySales AS (
    SELECT
        p.prod_subcategory,
        EXTRACT(YEAR FROM s.time_id) AS sales_year,
        SUM(s.amount_sold) AS total_sales,
        LAG(SUM(s.amount_sold)) OVER (PARTITION BY p.prod_subcategory ORDER BY EXTRACT(YEAR FROM s.time_id)) AS previous_year_sales
    FROM
        sales s
        JOIN products p ON s.prod_id = p.prod_id
    WHERE
        EXTRACT(YEAR FROM s.time_id) BETWEEN 1998 AND 2001
    GROUP BY
        p.prod_subcategory, sales_year
)
SELECT DISTINCT
    prod_subcategory
FROM
    SubcategorySales
WHERE
    total_sales > COALESCE(previous_year_sales, 0);